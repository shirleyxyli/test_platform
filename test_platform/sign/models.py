# -*- coding:utf8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
import socket
from datetime import datetime


class get_host_ip():
    def runner_test(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 80))
            ip = s.getsockname()[0]
        finally:
            s.close()
        return ip


# Create your models here.
class customers(AbstractUser):
    customers_id = models.AutoField(primary_key=True, verbose_name='用户ID')
    nickname = models.CharField(max_length=32, verbose_name='昵称')
    register_ip = models.CharField(max_length=15, verbose_name='注册ip地址')
    is_delete = models.BooleanField(default=False, verbose_name='是否弃用')
    telephone = models.IntegerField(default=0, verbose_name='电话号码')
    date_joined = models.CharField(max_length=50, default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    last_login = models.CharField(max_length=50, default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    class Meta:
        managed = True
        verbose_name = u'用户表'
        verbose_name_plural = verbose_name
        db_table = 'customers'
        unique_together = ('customers_id', 'username', 'register_ip')


class email_verify_record(models.Model):
    # 验证码
    code = models.CharField(max_length=20, verbose_name=u"验证码")
    email = models.EmailField(max_length=50, verbose_name=u"邮箱")
    # 包含注册验证和找回验证
    send_type = models.CharField(verbose_name=u"验证码类型", max_length=10,
                                 choices=(("register", u"注册"), ("forget", u"找回密码")))
    send_time = models.DateTimeField(verbose_name=u"发送时间")

    class Meta:
        verbose_name = u"邮箱验证码"
        verbose_name_plural = verbose_name
        db_table = 'email_verify_record'
        unique_together = ('code', 'email', 'send_type', 'send_time')

    def __str__(self):
        return '{0}({1})'.format(self.code, self.email)


class groups(models.Model):
    groups_id = models.CharField(max_length=50, verbose_name='团队ID')
    groups_name = models.CharField(max_length=50, verbose_name='团队名称')
    addtime = models.DateTimeField(verbose_name=u"添加时间", auto_now_add=True, null=True)

    class Meta:
        managed = True
        verbose_name = u'团队名称表'
        verbose_name_plural = verbose_name
        db_table = 'groups_name'
        unique_together = ('groups_id', 'groups_name', 'addtime')
