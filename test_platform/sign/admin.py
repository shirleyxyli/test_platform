#-*- coding:utf8 -*-
from django.contrib import admin
from sign import models

# Register your models here.
'''用户表'''
@admin.register(models.customers)
class customersAdmin(admin.ModelAdmin):
    list_display = ['customers_id','nickname','email','date_joined','is_staff','is_active','is_delete','register_ip']
    search_fields = ['username','email','register_ip','nickname','customers_id']
    list_filter = ['is_delete']
