# -*- coding:utf-8 -*-
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
from django.db.models import Q

class CustomBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, is_staff = None,is_active = None,**kwargs):
        try:
            user = User.objects.get(Q(username=username)|Q(email=username))
            if user.check_password(password):
                if is_staff is not None and is_active is not None :
                    if user.is_staff == 1 or user.is_active==1:
                        return user
                    else:
                        return None
        except Exception as e:  #可以捕获除与程序退出sys.exit()相关之外的所有异常
            return None