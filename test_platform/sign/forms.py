# -*- coding:utf-8 -*-
from django import forms
from django.forms import fields
from django.forms import widgets
from sign.models import *
import hashlib, sys


class LoginFrom(forms.Form):
    customers_username = fields.CharField(label='账号', required=True, widget=widgets.TextInput(
        attrs={'class': "login_input", 'placeholder': '请输入账号'}), min_length=4, max_length=20, strip=True,
                                          error_messages={'required': '账号不能为空', })
    customers_password = fields.CharField(label='密码', widget=widgets.PasswordInput(
        attrs={'class': "login_input", 'placeholder': '请输入密码'}), required=True, min_length=6, max_length=12, strip=True,
                                          error_messages={'required': '密码不能为空', })


class registerForm(forms.Form):
    customers_username = fields.CharField(label='用户名', required=True, widget=widgets.TextInput(
        attrs={'class': "login_input", 'placeholder': '请输入账号'}), min_length=4, max_length=12, strip=True,
                                          error_messages={'required': '必填项不能为空', })
    customers_email = fields.EmailField(label='邮箱地址', required=True,
                                        widget=widgets.EmailInput(attrs={'class': "login_input", 'placeholder': '请输入邮箱'}),
                                        min_length=6, max_length=30, error_messages={'required': '必填项不能为空', })
    customers_nickname = fields.CharField(label='名字', required=True, widget=widgets.TextInput(
        attrs={'class': "login_input", 'placeholder': '请输入姓名，如：张三'}), min_length=2, max_length=4)
    customers_password = fields.CharField(label='密码', widget=widgets.PasswordInput(
        attrs={'class': "login_input", 'placeholder': '请输入密码'}), required=True, min_length=6, max_length=12, strip=True,
                                          error_messages={'required': '必填项不能为空', })
    re_password = fields.CharField(label='确认密码',
                                   widget=widgets.PasswordInput(attrs={'class': "login_input", 'placeholder': '请再次输入密码'}),
                                   required=True, min_length=6, max_length=12, strip=True,
                                   error_messages={'required': '必填项不能为空', })
