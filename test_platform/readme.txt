
测试平台包括：
    (1) 接口测试
    (2) UI自动化
    (3) APP测试
    (4) 性能测试
    (5) 线上监控

目前该平台大部分支持windows系统，若需部署至linux，则烦请调整路径：

运行之前需要安装以下软件：
    python 3.6
    jdk 10
    jmeter 4.0
    Django (2.0.3)
    mysql 安装
    android环境 （android sdk  和 adb）
    redis
    RabbitMQ
    node.js
    nginx

通过pip install 安装的：
    pip install python-jenkins
    pip install openpyxl
    pip install requests
    pip install bottle
    pip install paramiko
    pip install python-jenkins
    pip install jsonpath
    pip install celery
    pip install django-celery
    pip install flower
    pip install -U celery_once
    pip install redis
    pip install django-celery-results
    pip install django-timezone-field
    pip install django-crontab  (linux下)
    xlrd (1.1.0)
    XlsxWriter (1.1.1)
    xlwt (1.3.0)

monkey部分：
    安装模拟器（建议夜神）
    真机（建议使用远程adb连接)
settings文件中需要设置的内容较多，请仔细查看：
    数据库地址
    发件邮箱配置
    各个文件路径设置
    远程服务器设置

django执行命令：
    python manage.py makemigrations sign
    python manage.py makemigrations personal_information
    python manage.py makemigrations task_list
    python manage.py migrate

celery命令：
    启动定时器：
        celery -A test_platform beat -l info
    启动worker:
        celery -A test_platform worker --loglevel=info --concurrency=1
    启动monkey执行队列：
        celery -A test_platform worker --loglevel=info --concurrency=1 -Q queue_monkey
    启动jmeter执行队列：
        celery -A test_platform worker --loglevel=info --concurrency=1 -Q queue_jmeter
    监控任务：
        celery flower -A test_platform --port=5556
