# -*- coding:utf8 -*-
from django.shortcuts import render, HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from sign.models import *
from task_list.views import *
from personal_information.models import *
import json


# Create your views here.
def index(request):
    if request.session.get('Login_type', None):
        return render(request, 'personal_information/index.html')


# 个人中心页
def customers_setting(request):
    context = menu_form(request)
    if context is not None:
        if request.method == "GET":
            customers_objects = customers.objects.filter(username=request.user)[0]
            email = customers_objects.email
            nickname = customers_objects.nickname
            last_login = customers_objects.last_login
            return render(request, "personal_information/customers_setting.html",
                          {'email': email, 'menu': context['menu'], 'username': context['username'],
                           'nickname': nickname, 'last_login': last_login})
        else:
            result = {u"code": 400, u"status": "0", u"message": "fail"}
            return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
    else:
        return HttpResponseRedirect(reverse('home'))


# 保存个人中心设置
def save_customers_setting(request):
    if request.session.get('Login_type', None):
        if request.method == "POST":
            email = request.POST['email']
            nickname = request.POST['nickname']
            # 先判断是否为空
            if email == "" or nickname == "":
                result = {u"code": 400, u"status": "0", u"message": "fail:昵称和邮箱不可为空"}
                return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
            # 先判断是否有重复的
            is_exit = "0"
            email_exit = "0"
            notcustomers = customers.objects.exclude(username=request.user)
            for x in notcustomers:
                if nickname == x.nickname:
                    is_exit = "1"
                if email == x.email:
                    email_exit = "1"
            if is_exit == "1":
                result = {u"code": 400, u"status": "0", u"message": "fail:昵称已经存在"}
                return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
            if email_exit == "1":
                result = {u"code": 400, u"status": "0", u"message": "fail:邮箱已经存在"}
                return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
            if is_exit == "0" and email_exit == "0":
                customers_objects = customers.objects.filter(username=request.user)[0]
                customers_objects.email = email
                customers_objects.nickname = nickname
                customers_objects.save()
                root_ojects = customers_root.objects.filter(customers_id=customers_objects.customers_id)[0]
                root_ojects.nickname = nickname
                root_ojects.save()
                dc_ojects = customers_dc.objects.filter(customers_id=customers_objects.customers_id)
                for i in dc_ojects:
                    i.nickname = nickname
                    i.save()
                result = {u"code": 200, u"status": "0", u"message": "success"}
                return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
        else:
            result = {u"code": 400, u"status": "0", u"message": "fail"}
            return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
    else:
        return HttpResponseRedirect(reverse('home'))


vip_name = {"0": "游客", "1": "普通用户", "2": "管理员用户", "3": "非测试管理用户", "A": "超级管理人员"}
cus_type = {"0": "非测试人员", "1": "测试人员"}


# 用户列表
def customers_list(request):
    context = menu_form(request)
    all_data = []
    if context is not None:
        if request.method == "GET":
            try:
                page_num = request.GET["page_num"]
                page_size = request.GET['page_size']
            except:
                page_num = 1
                page_size = 10
            try:
                team = request.GET["team"]
            except:
                team = ""
            try:
                username = request.GET["username"]
            except:
                username = ""

            if str(page_num) == "1":
                startPos = 0
            else:
                startPos = (int(page_num) - 1) * int(page_size)
            endPos = int(startPos) + int(page_size)
            customers_objects = customers.objects.filter(username=request.user)[0]
            # 判断是否是超级管理员
            cus_list = []
            is_sup = str(customers_objects.is_superuser)
            if is_sup != "True":
                return render(request, "errors/403.html")
            else:
                if team == "" and username == "":
                    root_ojects = customers_root.objects.filter().values_list('customers_id', 'nickname', 'groups_name',
                                                                              'vip', 'is_black', 'customers_type',
                                                                              'addtime').order_by('-id')
                elif team != "" and username == "":
                    root_ojects = customers_root.objects.filter(group_id=team).values_list('customers_id', 'nickname',
                                                                                           'groups_name',
                                                                                           'vip', 'is_black',
                                                                                           'customers_type',
                                                                                           'addtime').order_by('-id')
                elif team == "" and username != "":
                    root_ojects = customers_root.objects.filter(nickname__icontains=username).values_list('customers_id',
                                                                                               'nickname',
                                                                                               'groups_name',
                                                                                               'vip', 'is_black',
                                                                                               'customers_type',
                                                                                               'addtime').order_by(
                            '-id')
                else:
                    root_ojects = customers_root.objects.filter(nickname__icontains=username, group_id=team).values_list(
                            'customers_id', 'nickname', 'groups_name',
                            'vip', 'is_black', 'customers_type',
                            'addtime').order_by('-id')

                for x in root_ojects:
                    customers_data = customers.objects.filter(customers_id=x[0],is_delete=0)
                    for j in customers_data:
                        username = j.username
                        email = j.email
                        vips = vip_name[x[3]]
                        if x[4] is True:
                            black = "是"
                        else:
                            black = "否"
                        customer_type = cus_type[str(x[5])]
                        cus_list.append(
                                [x[0], username, x[1], x[2], vips, black, customer_type, x[6], email])
                total = len(cus_list)
                all_data.append(cus_list[int(startPos):int(endPos):1])
                return render(request, "personal_information/groups_setting.html",
                              {'data': all_data, 'menu': context['menu'], 'nickname': context['nickname'],
                               'username': context['username'], 'total': total})
        else:
            result = {u"code": 400, u"status": "0", u"message": "fail"}
            return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
    else:
        return HttpResponseRedirect(reverse('home'))


# 权限详情
def auth_detail(request):
    if request.session.get('Login_type', None):
        if request.method == "GET":
            customer_id = request.GET['customer_id']
            try:
                customers_data = customers_root.objects.filter(customers_id=customer_id)[0]
            except:
                result = {u"code": 400, u"status": "0", u"message": "fail"}
                return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
            cus_info = {}
            cus_info["group_id"] = customers_data.group_id
            cus_info["is_black"] = customers_data.is_black
            cus_info["vip"] = customers_data.vip
            cus_info["nickname"] = customers_data.nickname
            # 用户类型后期加入
            result = {u"code": 200, u"status": "1", u"message": "success", u"list": cus_info}
            return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
        else:
            result = {u"code": 400, u"status": "0", u"message": "fail"}
            return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
    else:
        return HttpResponseRedirect(reverse('home'))


# 配置权限
def add_auth(request):
    if request.session.get('Login_type', None):
        if request.method == "POST":
            customer_id = request.POST["customer_id"]
            group_id = request.POST["group_id"]
            is_black = request.POST["is_black"]
            vip = request.POST["vip"]
            nickname = request.POST["nickname"]
            # 判断nickname有无修改
            customers_objects = customers.objects.filter(customers_id=customer_id)[0]
            root_ojects = customers_root.objects.filter(customers_id=customer_id)[0]
            cur_nickname = customers_objects.nickname
            cur_group_id = root_ojects.group_id
            cur_is_black = root_ojects.is_black
            cur_vip = root_ojects.vip
            # 有修改时保存
            if nickname != cur_nickname:
                # 先判断是否有重复的
                is_exit = "0"
                notcustomers = customers.objects.exclude(username=request.user)
                for x in notcustomers:
                    if nickname == x.nickname:
                        is_exit = "1"
                if is_exit == "1":
                    result = {u"code": 400, u"status": "0", u"message": "fail:昵称已经存在"}
                    return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
                else:
                    customers_objects.nickname = nickname
                    customers_objects.save()
                    root_ojects.nickname = nickname
                    root_ojects.save()
                    dc_ojects = customers_dc.objects.filter(customers_id=customer_id)
                    for i in dc_ojects:
                        i.nickname = nickname
                        i.save()
            if cur_group_id != group_id:
                # 判断团队是否存在
                team_data = groups.objects.filter(groups_id=group_id)
                if team_data.count() > 0:
                    for j in team_data:
                        team_name = j.groups_name
                        root_ojects.group_id = group_id
                        root_ojects.groups_name = team_name
                        root_ojects.save()
                        dc_ojects = customers_dc.objects.filter(customers_id=customer_id)
                        for i in dc_ojects:
                            i.groups_name = team_name
                            i.save()
                        dc_login_ojects = dc_customers_login.objects.filter(customers_id=customer_id)
                        for t in dc_login_ojects:
                            t.group_id = group_id
                            t.save()
                else:
                    result = {u"code": 400, u"status": "0", u"message": "fail:团队不存在"}
                    return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
            if is_black != cur_is_black:
                root_ojects.is_black = is_black
                root_ojects.save()
                dc_ojects = customers_dc.objects.filter(customers_id=customer_id)
                for i in dc_ojects:
                    i.is_black = is_black
                    i.save()
                dc_login_ojects = dc_customers_login.objects.filter(customers_id=customer_id)
                for t in dc_login_ojects:
                    t.is_black = is_black
                    t.save()
            if vip != cur_vip:
                root_ojects.vip = vip
                root_ojects.save()
                dc_ojects = customers_dc.objects.filter(customers_id=customer_id)
                for i in dc_ojects:
                    i.vip = vip
                    i.save()
                dc_login_ojects = dc_customers_login.objects.filter(customers_id=customer_id)
                for t in dc_login_ojects:
                    t.vip = vip
                    t.save()
                # 判断是否设置为超管
                if vip == "A":
                    customers_objects.is_superuser = True
                    customers_objects.save()
                else:
                    customers_objects.is_superuser = False
                    customers_objects.save()
            # 用户类型后期加入
            result = {u"code": 200, u"status": "1", u"message": "success"}
            return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
        else:
            result = {u"code": 400, u"status": "0", u"message": "fail"}
            return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")
    else:
        return HttpResponseRedirect(reverse('home'))
