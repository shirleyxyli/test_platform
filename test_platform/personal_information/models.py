# -*- coding:utf8 -*-
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class sqlsetting(models.Model):
    customers_id = models.CharField(max_length=50,verbose_name='用户ID')
    database_key = models.CharField(max_length=50,verbose_name='数据库标志')
    database_url = models.CharField(max_length=50,verbose_name='数据库地址')
    database_port = models.CharField(max_length=50,verbose_name='数据库端口')
    database_account = models.CharField(max_length=50,verbose_name='数据库账号')
    database_password = models.CharField(max_length=50,verbose_name='数据库密码')
    database_name = models.CharField(max_length=50,verbose_name='数据库名称')
    database_id = models.CharField(max_length=50,verbose_name='数据库ID')
    status = models.CharField(max_length=50,verbose_name='删除状态')
    string_test3 = models.CharField(max_length=50,verbose_name='备用字段3')
    string_test4 = models.CharField(max_length=50,verbose_name='备用字段4')
    class Meta:
        managed = True
        verbose_name = u'数据库配置表'
        verbose_name_plural = verbose_name
        db_table = 'sqlsetting'
        unique_together = ('customers_id', 'database_id')

# -*- coding:utf8 -*-

class customers_root(models.Model):
    customers_id = models.IntegerField(verbose_name='用户ID')
    nickname = models.CharField(max_length=32, verbose_name='昵称')
    group_id = models.CharField(max_length=5,verbose_name='团队ID')#0:未分配;1:bg;2:ys;3:nc;A:超级管理员（黑名单记录为0）
    groups_name = models.CharField(max_length=50, verbose_name='团队名')
    is_black = models.BooleanField(default=False,verbose_name = '是否黑名单')#0:False;1:True; 是否黑名单
    vip = models.CharField(max_length=5,verbose_name='用户类型')#0:游客；1:普通用户;2:管理员用户;3:非测试管理用户;A:超级管理人员
    customers_type = models.IntegerField(verbose_name='用户类型1')#0：非测试人员；1：测试人员；
    is_changed = models.BooleanField(default=False, verbose_name='是否更改账号')
    addtime = models.DateTimeField(max_length=255, auto_now_add=True, null=True, verbose_name='增加时间')
    class Meta:
        managed = True
        verbose_name = u'用户权限表'
        verbose_name_plural = verbose_name
        db_table = 'customers_root'
        unique_together = ('customers_id', 'group_id','addtime')

class customers_info(models.Model):
    customers_id = models.IntegerField(verbose_name='用户ID')
    group_id = models.CharField(max_length=5, verbose_name='团队ID')  # 0:未分配;1:bg;2:ys;3:nc;A:超级管理员（黑名单记录为0）
    is_black = models.BooleanField(default=False, verbose_name='是否黑名单')  # 0:False;1:True; 是否黑名单
    vip = models.CharField(max_length=5, verbose_name='用户类型')  # 0:游客；1:普通用户;2:管理员用户;3:非测试管理用户;A:超级管理人员
    customers_type = models.IntegerField(verbose_name='用户类型1')  # 0：非测试人员；1：测试人员；
    domain = models.CharField(max_length=100, verbose_name='测试环境')
    sql_line = models.TextField(verbose_name='sql语句')
    addtime = models.DateTimeField(max_length=255, auto_now_add=True, null=True, verbose_name='增加时间')
    class Meta:
        managed = True
        verbose_name = u'个人用户信息表'
        verbose_name_plural = verbose_name
        db_table = 'customers_info'
        unique_together = ('customers_id', 'group_id','addtime')

class customers_dc(models.Model):
    customers_id = models.IntegerField(verbose_name='用户ID')
    nickname = models.CharField(max_length=32, verbose_name='昵称')
    last_login = models.DateTimeField(max_length=255, auto_now=True, null=True,verbose_name="最后登录时间")
    login_times = models.IntegerField(verbose_name='登录次数')
    execute_point_times = models.IntegerField(default=0, verbose_name='埋点任务执行次数')
    execute_performance_times = models.IntegerField(default=0, verbose_name='性能任务执行次数')
    execute_joggle_times = models.IntegerField(default=0, verbose_name='接口任务执行次数')
    execute_business_times = models.IntegerField(default=0, verbose_name='业务任务执行次数')
    execute_others_times = models.IntegerField(default=0, verbose_name='其他任务执行次数')
    has_point_file = models.IntegerField(default=0, verbose_name='埋点任务拥有数')
    has_performance_file = models.IntegerField(default=0, verbose_name='性能任务拥有数')
    has_joggle_file = models.IntegerField(default=0, verbose_name='接口任务拥有数')
    has_business_file = models.IntegerField(default=0, verbose_name='业务任务拥有数')
    has_others_file = models.IntegerField(default=0, verbose_name='其他任务拥有数')
    groups_name = models.CharField(max_length=50, verbose_name='团队名')
    is_black = models.BooleanField(default=False, verbose_name='是否黑名单')  # 0:False;1:True; 是否黑名单
    vip = models.CharField(max_length=5, verbose_name='用户类型')  # 0:游客；1:普通用户;2:管理员用户;3:非测试管理用户;A:超级管理人员
    customers_type = models.IntegerField(verbose_name='用户类型1')  # 0：非测试人员；1：测试人员；
    addtime = models.CharField(max_length=255, verbose_name='增加时间')
    class Meta:
        managed = True
        verbose_name = u'用户信息采集表'
        verbose_name_plural = verbose_name
        db_table = 'customers_dc'
        unique_together = ('customers_id', 'nickname','groups_name','addtime')

class dc_customers_login(models.Model):
    customers_id = models.IntegerField(verbose_name='用户ID')
    group_id = models.CharField(max_length=5, verbose_name='团队ID')  # 0:未分配;1:bg;2:ys;3:nc;A:超级管理员（黑名单记录为0）
    last_login = models.DateTimeField(max_length=255, auto_now=True, null=True, verbose_name='最后登录时间')
    login_times = models.IntegerField(verbose_name='登录次数')
    is_black = models.BooleanField(default=False, verbose_name='是否黑名单')  # 0:False;1:True; 是否黑名单
    vip = models.CharField(max_length=5, verbose_name='用户类型')  # 0:游客；1:普通用户;2:管理员用户;3:非测试管理用户;A:超级管理人员
    customers_type = models.IntegerField(verbose_name='用户类型1')  # 0：非测试人员；1：测试人员；
    addtime = models.DateTimeField(max_length=255, auto_now_add=True, null=True, verbose_name='增加时间')
    class Meta:
        managed = True
        verbose_name = u'用户登录信息采集表'
        verbose_name_plural = verbose_name
        db_table = 'dc_customers_login'
        unique_together = ('customers_id', 'group_id','addtime')

class dc_customers_worker(models.Model):
    customers_id = models.IntegerField(verbose_name='用户ID')
    group_id = models.CharField(max_length=5, verbose_name='团队ID')  # 0:未分配;1:bg;2:ys;3:nc;A:超级管理员（黑名单记录为0）
    execute_point_times = models.IntegerField(default=0,verbose_name='埋点任务执行次数')
    execute_performance_times = models.IntegerField(default=0,verbose_name='性能任务执行次数')
    execute_joggle_times = models.IntegerField(default=0,verbose_name='接口任务执行次数')
    execute_business_times = models.IntegerField(default=0,verbose_name='业务任务执行次数')
    execute_others_times = models.IntegerField(default=0,verbose_name='其他任务执行次数')
    has_point_file = models.IntegerField(default=0,verbose_name='埋点任务拥有数')
    has_performance_file = models.IntegerField(default=0,verbose_name='性能任务拥有数')
    has_joggle_file = models.IntegerField(default=0,verbose_name='接口任务拥有数')
    has_business_file = models.IntegerField(default=0,verbose_name='业务任务拥有数')
    has_others_file = models.IntegerField(default=0,verbose_name='其他任务拥有数')
    is_black = models.BooleanField(default=False, verbose_name='是否黑名单')  # 0:False;1:True; 是否黑名单
    vip = models.CharField(max_length=5, verbose_name='用户类型')  # 0:游客；1:普通用户;2:管理员用户;3:非测试管理用户;A:超级管理人员
    customers_type = models.IntegerField(verbose_name='用户类型1')  # 0：非测试人员；1：测试人员；
    addtime = models.DateTimeField(max_length=255, auto_now_add=True, null=True, verbose_name='增加时间')
    class Meta:
        managed = True
        verbose_name = u'用户任务信息采集表'
        verbose_name_plural = verbose_name
        db_table = 'dc_customers_worker'
        unique_together = ('customers_id', 'group_id','addtime')