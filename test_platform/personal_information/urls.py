# -*- coding:utf8 -*-
from django.urls import path
from personal_information import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
                  path('', views.index, name='PI_index'),
                  path('info/', views.customers_setting, name='customers_setting'),
                  path('info/revise/', views.save_customers_setting, name='save_customers_setting'),
                  path('info/auth/', views.customers_list, name='customers_list'),
                  path('info/auth/detail/', views.auth_detail, name='auth_detail'),
                  path('info/auth/add/', views.add_auth, name='add_auth'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
