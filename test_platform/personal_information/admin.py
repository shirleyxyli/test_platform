#-*- coding:utf8 -*-
from django.contrib import admin
from personal_information import models

# Register your models here.
'''用户信息权限表'''
@admin.register(models.customers_root)
class customers_rootAdmin(admin.ModelAdmin):
    list_display = ['customers_id','nickname','groups_name','is_black','vip','customers_type','is_changed','addtime']
    search_fields = ['groups_name','nickname','customers_id']
    list_per_page = 20
    ordering = ['-addtime']
    date_hierarchy = 'addtime'